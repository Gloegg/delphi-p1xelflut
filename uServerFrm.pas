unit uServerFrm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  IdBaseComponent, IdComponent, IdUDPBase, IdUDPServer, IdGlobal,
  IdSocketHandle,
  Vcl.AppEvnts, System.Diagnostics;

type
  TForm1 = class(TForm)
    udpServer: TIdUDPServer;
    ApplicationEvents1: TApplicationEvents;
    imgPixel: TImage;
    lbInfo: TLabel;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure udpServerUDPRead(AThread: TIdUDPListenerThread;
      const AData: TIdBytes; ABinding: TIdSocketHandle);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private-Deklarationen }
    sw: TStopWatch;
    fFrames: integer;
    fPixels: integer;
    pixels: array of TColor;
    procedure ShowFPS;
  public
    { Public-Deklarationen }
    procedure Render;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses
  WinSock;

Function GetIPAddress():String;
type
  pu_long = ^u_long;
var
  varTWSAData : TWSAData;
  varPHostEnt : PHostEnt;
  varTInAddr : TInAddr;
  namebuf : Array[0..255] of Ansichar;
begin
  If WSAStartup($101,varTWSAData) <> 0 Then
  Result := 'No. IP Address'
  Else Begin
    gethostname(namebuf,sizeof(namebuf));
    varPHostEnt := gethostbyname(namebuf);
    varTInAddr.S_addr := u_long(pu_long(varPHostEnt^.h_addr_list^)^);
    Result := inet_ntoa(varTInAddr);
  End;
  WSACleanup;
end;

procedure TForm1.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
  Render;
  ShowFPS;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  fPixels := 0;
  udpServer.Active := true;
  lbInfo.Caption := lbInfo.Caption + GetIPAddress;
  setlength(pixels, imgPixel.Height * imgPixel.Width);
  sw.Start;
end;

procedure TForm1.Render;
type
  PPixArray = ^TPixArray;
  TPixArray = array [1 .. 4] of Byte;
var
  p: PPixArray;
  bmp: TBitmap;
begin
  bmp := TBitmap.Create;
  bmp.PixelFormat := pf32bit;
  bmp.SetSize(imgPixel.Width, imgPixel.Height);
  p := bmp.Scanline[bmp.Height - 1]; // starting address of "last" row
  Move(pixels[0], p^, bmp.Width * bmp.Height * SizeOf(TColor));
  imgPixel.Picture.Assign(bmp);
  bmp.Free;
  inc(fFrames);
end;

procedure TForm1.ShowFPS;
begin
  if sw.ElapsedMilliseconds >= 1000 then
  begin
    Caption := Format('p1xelflut FPS: %d', [fFrames]);
    fFrames := 0;
    sw.Reset;
    sw.Start;
  end;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  lbInfo.Visible := false;
  Timer1.Enabled := false;
end;

procedure TForm1.udpServerUDPRead(AThread: TIdUDPListenerThread;
  const AData: TIdBytes; ABinding: TIdSocketHandle);
var
  s: string;
  sl: TStringList;
  x, y: integer;
  color: TColor;
begin
  if Length(AData) < 25 then
  begin
    s := BytesToString(AData);
    sl := TStringList.Create;
    sl.Delimiter := ' ';
    sl.StrictDelimiter := true;
    sl.DelimitedText := s;

    if sl[0] = 'PX' then
    begin
      x := StrToIntDef(sl[1], -1);
      if (x < 0) or (x >= imgPixel.Width) then
      begin
        sl.Free;
        exit;
      end;
      y := StrToIntDef(sl[2], -1);
      if (y < 0) or (y >= imgPixel.Height) then
      begin
        sl.Free;
        exit;
      end;
      color := StrToIntDef('$' + sl[3], 0);
      pixels[(imgPixel.Height - y - 1) * imgPixel.Width + x] := color;
    end
    else if sl[0] = 'SIZE' then
    begin
      udpServer.Send(ABinding.PeerIP, 1234,
        Format('%d %d', [imgPixel.Width, imgPixel.Height]));
    end
    else if sl[0] = 'INFO' then
    begin
      lbInfo.Visible := true;
      Timer1.Enabled := true;
    end;

    sl.Free;
  end;
end;

end.

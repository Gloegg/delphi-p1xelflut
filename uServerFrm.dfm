object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'p1xelflut'
  ClientHeight = 480
  ClientWidth = 640
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object imgPixel: TImage
    Left = 0
    Top = 0
    Width = 640
    Height = 480
    Align = alClient
    ExplicitLeft = 8
    ExplicitTop = 8
    ExplicitWidth = 645
    ExplicitHeight = 426
  end
  object lbInfo: TLabel
    Left = 8
    Top = 8
    Width = 209
    Height = 35
    Caption = 'UDP p1xelflut @'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -29
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = False
    Visible = False
  end
  object udpServer: TIdUDPServer
    Bindings = <
      item
        IP = '0.0.0.0'
        Port = 1234
      end>
    DefaultPort = 0
    ThreadedEvent = True
    OnUDPRead = udpServerUDPRead
    Left = 56
    Top = 56
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 160
    Top = 232
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = Timer1Timer
    Left = 312
    Top = 248
  end
end
